<map version="freeplane 1.6.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Objekt-Farb-Zusammenh&#xe4;nge beim visuellen Grounding" LOCALIZED_STYLE_REF="AutomaticLayout.level.root" FOLDED="false" ID="ID_1310616124" CREATED="1566217610985" MODIFIED="1566306719576" STYLE="bubble">
<font SIZE="25" BOLD="true"/>
<hook NAME="MapStyle" zoom="0.685">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="2" RULE="ON_BRANCH_CREATION"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="HEADINGS"/>
<node TEXT="Interaktion zwischen Bottom-Up- und Top-Down-Prozessen" FOLDED="true" POSITION="right" ID="ID_113389585" CREATED="1566222329330" MODIFIED="1566306719582" STYLE="bubble">
<font SIZE="18" BOLD="true"/>
<edge COLOR="#7c007c"/>
<node TEXT="M&#xf6;gliche Einfl&#xfc;sse von Top-Down-Informationen auf Bottom-Up-Wahrnehmung: Wissen &#xfc;ber typische Objektfarben, Wissen &#xfc;ber typische Farbwerte einer Objektfarbe" ID="ID_1987614182" CREATED="1566222348453" MODIFIED="1566306719582" STYLE="bubble">
<font SIZE="18"/>
</node>
<node TEXT="Implizite Top-Down-Informationen bereits beim Bottom-Up-Klassifikator? Objektspezifische Farbauspr&#xe4;gungen?" ID="ID_152371112" CREATED="1566222551545" MODIFIED="1566306719583" STYLE="bubble">
<font SIZE="18"/>
</node>
<node TEXT="Formen von Top-Down-Wissen bei Farbbenennung? (hier: Klassenzugeh&#xf6;rigkeit; auch andere Objektinformationen?)" ID="ID_1212418741" CREATED="1566222664387" MODIFIED="1566306719584" STYLE="bubble">
<font SIZE="18"/>
</node>
<node TEXT="Temporale Struktur der Bottom-Up- und Top-Down-Prozesse" FOLDED="true" ID="ID_1046108910" CREATED="1566219000778" MODIFIED="1566306719584" STYLE="bubble">
<font SIZE="18"/>
<node TEXT="Unterschiedliche Theorien f&#xfc;hren zu unterschiedlichen Annahmen:&#xa;Mitterer: BottomUp -&gt; TopDown, da ansonsten BU-Infos durch TD-Infos &#xfc;berlagert werden k&#xf6;nnen (&apos;hallucinations&apos;)&#xa;G&#xe4;rdenfors: Simultan oder Topdown -&gt; Bottomup, da relevanter Bereich der Conceptual Spaces durch TD-Wissen bestimmt wird (oder auch Bottomup-&gt;Topdown?)" ID="ID_343192599" CREATED="1566290366265" MODIFIED="1566306719585" STYLE="bubble">
<font SIZE="18"/>
</node>
</node>
</node>
<node TEXT="Wesentliche Ergebnisse der Versuche" FOLDED="true" POSITION="left" ID="ID_561138962" CREATED="1566292728719" MODIFIED="1566306719585" STYLE="bubble">
<font SIZE="18" BOLD="true"/>
<edge COLOR="#ff0000"/>
<node TEXT="BU-Klassifikation" FOLDED="true" ID="ID_948175299" CREATED="1566292839553" MODIFIED="1566306719585" STYLE="bubble">
<font SIZE="18"/>
<node TEXT="MLP als geeignete Architektur, RGB-Farbhistogramme als geeignete Repr&#xe4;sentation" ID="ID_1807052270" CREATED="1566296781513" MODIFIED="1566306719586" STYLE="bubble">
<font SIZE="18"/>
</node>
</node>
<node TEXT="Late Fusion-Modell" FOLDED="true" ID="ID_499492092" CREATED="1566296808698" MODIFIED="1566306719586" STYLE="bubble">
<font SIZE="18"/>
<node TEXT="Top-Down-Klassifikator f&#xfc;hrt zu Effekten &#xe4;hnlich des Memory Color Effects, Kombination f&#xfc;hrt zu Farbkonstanz-Effekt, wenn Objekte kanonische Farbe haben" ID="ID_871485410" CREATED="1566296824009" MODIFIED="1566306719586" STYLE="bubble">
<font SIZE="18"/>
</node>
<node TEXT="Allgemein zeigt kombiniertes Modell konsistent verbesserte Leistung" ID="ID_134098758" CREATED="1566296919003" MODIFIED="1566306719587" STYLE="bubble">
<font SIZE="18"/>
</node>
<node TEXT="Probleme: Kann nicht-prototypische Farben schlecht erfassen, Fragw&#xfc;rdige Repr&#xe4;sentation der objektspezifischen Farbtendenzen, basiert zu einem hohen Ma&#xdf; auf nicht-gegroundeten Informationen" ID="ID_1533813675" CREATED="1566296977804" MODIFIED="1566306719588" STYLE="bubble">
<font SIZE="18"/>
</node>
</node>
<node TEXT="Early Fusion-Modell" FOLDED="true" ID="ID_394137749" CREATED="1566297033884" MODIFIED="1566306719588" STYLE="bubble">
<font SIZE="18"/>
<node TEXT="Ergebnisse allgemein &#xe4;hnlich wie Late Fusion" ID="ID_524208699" CREATED="1566297041115" MODIFIED="1566306719588" STYLE="bubble">
<font SIZE="18"/>
</node>
<node TEXT="Embeddings als geeignete Repr&#xe4;sentation von Objekt-Farb-Tendenzen" ID="ID_538003698" CREATED="1566297060299" MODIFIED="1566306719589" STYLE="bubble">
<font SIZE="18"/>
</node>
<node TEXT="Mehr Einfluss der Bottom-Up-Komponente bei CDOs, erfasst mehr nicht-kanonische Farben" ID="ID_1937395776" CREATED="1566297076923" MODIFIED="1566306719590" STYLE="bubble">
<font SIZE="18"/>
</node>
<node TEXT="grunds&#xe4;tzliches Problem mit nicht-gegroundeten Informationen bleibt bestehen" ID="ID_1552152411" CREATED="1566297114428" MODIFIED="1566306719590" STYLE="bubble">
<font SIZE="18"/>
</node>
</node>
<node TEXT="VGG-Klassifikation" FOLDED="true" ID="ID_225983741" CREATED="1566297130892" MODIFIED="1566306719590" STYLE="bubble">
<font SIZE="18"/>
<node TEXT="Einfaches MLP zeigt Charakteristika von Bottom-Up-Klassifikator, ein Multi-Task-Modell, das explizite Klassenzuweisung beinhaltet, zeigt Charakteristika eines TD-Klassifikators. VGG-Features lassen sich also im Sinne einer Bottom-Up-Verarbeitung interpretieren, Top-Down-Informationen k&#xf6;nnen daraus gewonnen werden" ID="ID_1137097445" CREATED="1566297146204" MODIFIED="1566306719591" STYLE="bubble">
<font SIZE="18"/>
</node>
<node TEXT="Late Fusion-Modell aus Multitask-Klassifikator und Bottomup-Klassifikator ist den einzelnen Klassifikatoren &#xfc;berlegen: Top-Down-Augmentierung ist auch dann zutr&#xe4;glich, wenn die Informationen aus dem visuellen Input abgeleitet werden und die Qualit&#xe4;t vergleichsweise schlecht ist" ID="ID_831382368" CREATED="1566297264845" MODIFIED="1566306719591" STYLE="bubble">
<font SIZE="18"/>
</node>
</node>
</node>
<node TEXT="Ausblick" FOLDED="true" POSITION="left" ID="ID_1552750647" CREATED="1566223353652" MODIFIED="1566306719592" STYLE="bubble">
<font SIZE="18" BOLD="true"/>
<edge COLOR="#ff0000"/>
<node TEXT="Beitr&#xe4;ge durch die Masterarbeit" FOLDED="true" ID="ID_616657911" CREATED="1566218038100" MODIFIED="1566306719592" STYLE="bubble">
<font SIZE="18"/>
<node TEXT="Erkenntnisse aus der Farbpsychologie operationalisiert und ihre Anwendbarkeit und Sinnhaftigkeit im visuellen Grounding von Farbw&#xf6;rtern in nat&#xfc;rlichen Bildern demonstriert" FOLDED="true" ID="ID_1290737823" CREATED="1566218072679" MODIFIED="1566306719592" STYLE="bubble">
<font SIZE="18"/>
<node TEXT="In der Farbpsychologie relevante Konzepte wie Color Diagnosticity" ID="ID_1540549720" CREATED="1566306650961" MODIFIED="1566306719593" STYLE="bubble">
<font SIZE="18"/>
</node>
<node TEXT="Relevanz der Betrachtung von Top-Down-Prozessen (wie z.B. der Memory Color Effect): Beitr&#xe4;ge zur Farbkonstanz" ID="ID_557462268" CREATED="1566306665074" MODIFIED="1566306719594" STYLE="bubble">
<font SIZE="18"/>
</node>
<node TEXT="Getrennte Betrachtung von Bottom-Up- und Top-Down-Prozessen, Untersuchung der Interaktion dieser Prozesse" ID="ID_1656600313" CREATED="1566306706068" MODIFIED="1566306719573" STYLE="bubble">
<font SIZE="18"/>
</node>
</node>
</node>
<node TEXT="Verbesserungspotential?" FOLDED="true" ID="ID_1452848896" CREATED="1566220202048" MODIFIED="1566306719595" STYLE="bubble">
<font SIZE="18"/>
<node TEXT="auch BU-Rekalibrierung (wie bei Zarrie&#xdf; &amp; Schlangen 2016, Mojsilovic) - in der Farbwahrnehmung BU+TD-Rekalibrierung" ID="ID_1568060648" CREATED="1566220214755" MODIFIED="1566306719595" STYLE="bubble">
<font SIZE="18"/>
</node>
<node TEXT="Verbindung mit anderen Arbeiten zum Visuellen Grounding von Farbw&#xf6;rtern: Pragmatische Faktoren (z.B. von Farbe als distinktives Objektmerkmal);  Content Selection, Salienz" ID="ID_1609161769" CREATED="1566220530938" MODIFIED="1566306719596" STYLE="bubble">
<font SIZE="18"/>
</node>
<node TEXT="Viel Verbesserungspotential bei den einzelnen Klassifikatoren: Hier Fokus auf Vergleichbarkeit, nicht auf Leistung" ID="ID_1071181220" CREATED="1566290523707" MODIFIED="1566306719597" STYLE="bubble">
<font SIZE="18"/>
</node>
<node TEXT="Generalisierung / Strukturierung des TD-Wissens: Etwa Ontologien statt Objektklassen" ID="ID_132999222" CREATED="1566304025345" MODIFIED="1566306719597" STYLE="bubble">
<font SIZE="18"/>
</node>
</node>
<node TEXT="&#xdc;bertragbarkeit auf andere Ph&#xe4;nomenbereiche" FOLDED="true" ID="ID_122569138" CREATED="1566218957379" MODIFIED="1566306719598" STYLE="bubble">
<font SIZE="18"/>
<node TEXT="Ber&#xfc;cksichtigung von Top-Down-Prozessen nicht nur in Bezug auf Farbw&#xf6;rter relevant; auch bei anderen Aufgaben" ID="ID_494110956" CREATED="1566219176326" MODIFIED="1566306719598" STYLE="bubble">
<font SIZE="18"/>
</node>
</node>
</node>
<node TEXT="Kognitive Plausibilit&#xe4;t" FOLDED="true" POSITION="right" ID="ID_133007916" CREATED="1566217730027" MODIFIED="1566306719581" STYLE="bubble">
<font SIZE="18" BOLD="true"/>
<edge COLOR="#0000ff"/>
<node TEXT="Ist das Modell mit vorgeschalteter Objekterkennung plausibel?" ID="ID_1924564553" CREATED="1566217791756" MODIFIED="1566306719581" STYLE="bubble">
<font SIZE="18"/>
</node>
<node TEXT="Wege zu einem kognitiv plausibleren Modell?" ID="ID_1070266086" CREATED="1566217839868" MODIFIED="1566306719582" STYLE="bubble">
<font SIZE="18"/>
</node>
</node>
<node TEXT="Wortbedeutungen" FOLDED="true" POSITION="right" ID="ID_825936204" CREATED="1566218986004" MODIFIED="1566306719577" STYLE="bubble">
<font SIZE="18" BOLD="true"/>
<edge COLOR="#7c7c00"/>
<node TEXT="Notwendigkeit einer flexiblen / situativen Farbwortsemantik: Farbwort-Zuweisung nicht nur Bottom-Up-getrieben (wie nach Kay), sondern abh&#xe4;ngig von externen Faktoren und z.T. objektabh&#xe4;ngig (wie z.B. nach G&#xe4;rdenfors)" ID="ID_430622560" CREATED="1566220306763" MODIFIED="1566306719577" STYLE="bubble">
<font SIZE="18"/>
</node>
<node TEXT="Geh&#xf6;ren Objektinformationen zur gegroundeten Wortbedeutung oder helfen sie nur bei der Zuweisung / der Rekalibrierung der Wahrnehmung?" ID="ID_1693252012" CREATED="1566220153297" MODIFIED="1566306719578" STYLE="bubble">
<font SIZE="18"/>
</node>
<node TEXT="Wie / Wo ist in den Modellen die Bedeutung von Farbw&#xf6;rtern repr&#xe4;sentiert?" FOLDED="true" ID="ID_131302549" CREATED="1566220634132" MODIFIED="1566306719579" STYLE="bubble">
<font SIZE="18"/>
<node TEXT="BottomUp-Modelle: Parallelen zum Farbmodell von Kay: Farbreizen (etwa in Form von Farb-Histogrammen) werden Appropriateness Scores f&#xfc;r Farbkategorien zugewiesen. Die Bedeutung der Farbkategorien l&#xe4;sst sich demnach anhand ihrer Denotation als Fuzzy-Sets beschreiben (-&gt; Bereiche im Farbraum). Evidenz: &#xc4;hnliche Ergebnisse f&#xfc;r Pixelwise-Klassifikatoren wie f&#xfc;r BU-Perzeptron; in Pixelwise-Klassifikatoren ist dieses Prinzip sehr direkt umgesetzt" ID="ID_1770070357" CREATED="1566220648729" MODIFIED="1566306719579" STYLE="bubble">
<font SIZE="18"/>
</node>
<node TEXT="TopDown-Modelle: Farbe wird durch Zugeh&#xf6;rigkeit zu Objekten definiert: Farben werden Zugeh&#xf6;rigkeitswerte f&#xfc;r Objektkategorien zugewiesen - Parallelen zu Kubat, Mitterer, Witzel&amp;Gegenfurtner" ID="ID_1416362984" CREATED="1566220760482" MODIFIED="1566306719580" STYLE="bubble">
<font SIZE="18"/>
</node>
<node TEXT="Kombinierte Klassifikatoren - schwieriger zu Bestimmen, da komplexe / multimodale Repr&#xe4;sentationen notwendig. G&#xe4;rdenfors-like bei EarlyFusion-Klassifikator? Was ist mit Late Fusion?" ID="ID_994216650" CREATED="1566221494408" MODIFIED="1566306719580" STYLE="bubble">
<font SIZE="18"/>
</node>
</node>
</node>
</node>
</map>
