# To Do-Liste Masterarbeit


**Schreiben**
----------------

### Einleitung

- Formulieren
- Ziel, Fragen:
  - Können Top-Down-Informationen (etwa über Memory Colors) aus einem Datenset wie VisualGenome gewonnen werden?
  - Können durch ein Modell, das Bottom-Up- und Top-Down-Informationen berücksicht, Effekte wie der Memory Color Effekt repliziert werden?
  - Ist die Miteinbeziehung von Top-Down-Informationen hilfreich für Color Constancy?
  - Kann ein kombiniertes Modell Objektfarben bestimmen, die von der prototypischen Farbe abweichen?

  - Grounding:
    - lexical semantics anchor in perceptual variables
    - hier: Verankerung durch Klassifikatoren, die passende Kategorien-Labels für visuellen Input bestimmen

### Theoretischer Teil
#### Grounding
#### Farbe und Farbwahrnehmung
##### Was ist Farbe?
##### Digitale Repräsentation von Farbe
##### Bottom-Up- und Top-Down-Prozesse in der menschlichen Farbwahrnehmung
###### Bottom-Up und Top-Down
###### Bottom-Up-Verarbeitung von Farbreizen
###### Top-Down-Prozesse bei der Wahrnehmung von Objektfarben
- Linguistic word choice depends on context as well as the actual sensor data. For example, the meaning of a modifier may depend on the category of objects it is modifying, thus the visual association of red shifts widely as it is used in differing contexts such as red wine, red hair or red car. (royreiter2005, S. 4)
  - prototypsiche Farbgebung kann variieren zwischen Objekten, nicht _ein_ Farb-Prototyp, wie etwa von kay1978 angenommen -> Top Down Farbwahrnehmung
  - verwandt mit ambigen Objektfarben? -> Objektspezifische Interpretation von Farbreizen
##### Farbwörter in den Sprachen der Welt
##### Semantik von Farbwörtern
- Unterschied Gärdenfors / Barsalou: Conceptual spaces, as proposed by Grdenfors, are similar to entity embeddings, but provide more structure. In conceptual spaces, among others, dimensions are interpretable and grouped into facets, and properties and concepts are explicitly modelled as (vague) regions.
- In Theorie von Gärdenfors kann gewisse Trennung zwischen den visuellen und konzeptuellen Ebenen interpretiert werden, da die visuellen Informationen in diesem Framework bereits als interpretierbare Quality Dimensions vorliegen
#### Vorausgehende Arbeiten
- Unterschied zarriessschlangen2016 und vorliegende Arbeit: Hier soll (zumindest im ersten Schritt) eine explizite Trennung zwischen Bottom-Up- und Top-Down-Prozessen vorgenommen werden, um den jeweiligen Einfluss der Verarbeitungsstrategien zu erfassen
### Versuche
#### Daten
##### Sampling:
##### CDOs/CNOs:
- Quelle Entropie
#### Vorversuch: BottomUp-Klassifikation
- ~~Backpropagation~~
- ~~Formel für benavente2006~~
- Precision/Recall raus, wenn später nicht diskutiert
- Klassifikationsproblem Farbklassen
- Tabelle Farbmodelle
#### Kombinierter Klassifikator mit bekannten Objektklassen (Gold Labels, Late Fusion)
- ~~Beschreibung Naive Bayes Klassifikator: Satz von Bayes erwähnen~~
- ~~Repräsentation von Objekt-Farbtendenzen: Ein bisschen besser formulieren~~
- Klassifikationsproblem

- Diskussion:
  - zarriessschlangen2016: Die Rekalibrierung ändert die Vorhersagen des allgemeinen Klassifikators vor allem dann, wenn die Klassifikation der Farbhistogramme zu ambivalenten Vorhersagen führt. Ist das hier auch der Fall?
  - weiterer Kritikpunkt: Klassifikationsresultate der einzelnen Komponenten sind identisch in ihrer Form (Membership Values über Farbkategorien). Dabei unterscheiden sich die Vorhersagen qualitativ voneinander: Der Bottom-Up-Klassifikator trifft ein Urteil darüber, wie adäquat die Beschreibung eines Farbreizes durch ein Label ist. Der Top-Down-Klassifikator sagt dagegen aus, wie wahrscheinlich es ist dass ein Objekt mit einer bestimmten Farbe beschrieben wird. Unterschied dabei: Zum Beispiel, dass ähnliche Werte für zwei Farbkategorien durch den Bottom-Up-Klassifikator auf ambige Farben hinweisen, die möglicherweise durch mehrere Farbwörter adäquat beschrieben werden können. Beim Top-Down-Klassifikator würden ähnliche Werte für zwei Farbklassen dagegen darauf hinweisen, dass das betreffende Objekt mit einer ähnlichen Wahrscheinlichkeit eine der beiden Farben aufweist. Im Gegensatz zum Bottom-Up-Klassifikator lassen sich hierbei jedoch keine qualitativen Schlüsse auf die jeweilige Farbe ziehen.
  - Klarstellen, dass TopDown-NaiveBayes nicht als Grounding verstanden werden kann: Keine Interpretation von perzeptuellen Informationen -> Keine Verknüpfung zwischen Sprache und Welt

#### Kombinierter Klassifikator mit bekannten Objektklassen (Gold Labels, Early Fusion)

- Embeddings: Modell für Teil von komplexen Repräsentationen von Objekt-Konzepten, Ausschnitt des konzeptionellen Wissens über Objekte. Kategoriale Objektklassen dagegen nur Labels ohne inhärente Bedeutung
- (Evtl Tabelle: Überschneidung mit BU-Modell für Late- und Early-Fusion mit jeweiliger Accuracy)
- Farb-Bias (Beispiele)

#### Kombinierter Klassifikator mit unbekannten Objektklassen (keine Gold Labels)

-  ~~Outputs in Schaubild~~
- (VGG-Features als höherstufigere visuelle Repräsentationen?)
- Farb-Bias (Beispiele)

### Diskussion

  - Farbwort-Repräsentationen in den Modellen (aka die gegroundete Bedeutung von Farbwörtern, wie sie in den Modellen repräsentiert ist):
      - Top-Down-NB: Keine Aussage über die Bedeutung von Farbwörtern im eigentlichen Sinne, statistische Aussage über die Wahrscheinlichkeit von Objektfarben
    - Unterscheidung zwischen Late Fusion und Early Fusion bei der kombinierten Klassifikation:
      - Bei Late Fusion-Modell sind Objektinformationen kein Teil der Repräsentation von Farbwörtern. Die Kontexteffekte entstehen erst nach der Klassenzuweisung für den visuellen Input, also erst nachdem der Input mit den konnektionistisch repräsentierten Farbwort-Konzepten abgeglichen wurde.
      - Bei Early Fusion-Modell werden die Repräsentationen dagegen unter Einbeziehung des visuellen Inputs und des kontextuellen Wissens über Farbtendenzen der betrachteten Objekte erlernt: Objektinformationen stellen demnach einen inhärenten Bestandteil der Farbwort-Repräsentationen dar.
        - Intuitive Analogie: "Grün" ist für den Klassifikator ein Farbreiz mit den RGB-Werten (0,200,0), aber auch "die Farbe, die ein Baum hat"

### Fazit

### Literatur


**Programmieren**
----------------
****************

**Sonstiges**
----------------
****************

- Repräsentationen in KNNs: Aktivationen / Aktivationsmuster im Netz
- Late Fusion: Semantische Rekalibrierung, Early Fusion: Perzeptuelle Rekalibrierung

# ToDo Tagesaktuell

Buch:

Evaluating learning algorithms : a classification perspective / Nathalie Japkowicz ; Mohak Shah
FB 10: Mathematik, Informatik	HK632 J35

# Zitat-Sammelbox
- kubat2009: meaning of color names involves precisely the same brain regions and representations as perception of color. In other words, the mechanisms of color perception are an intrinsic part of semantic knowledge. Conversely, this view also suggests that semantic knowledge is an intrinsic part of color perception. [...] top-down information (such as semantic knowledge) directly influences lowerlevel perceptual processes. Visual perception, in particular, is a highly interactive process. The perception of illusory contours is guided by top-down expectations (Lee & Nguyen, 2001) and recognition of objects is influenced by scene contexts (Bar, 2004). Similarly, color perception appears to be influenced by memory of an object’s color: when participants were asked to adjust the color of a fruit object until it appeared achromatic, their judgments were consistently shifted away from gray in a direction opposite to the typical color of the fruit (Hansen, Olkkonen, Sebastian, & Gegenfurtner, 2006). That is, the participants’ perception of the color of a displayed fruit was biased by the conceptual knowledge of the typical color of that fruit.
