#!/bin/bash

# Data Extraction
cd ../Preprocessing/
python data-extraction.py

# Histogramme generieren für bgr
cd histogram_color/
python preprocessing-baseline.py

cd ../type_color
python preprocessing-types.py

# Pixelwise-Klassifikation
cd ../../Modelle/pixelwise_rgb_color/
python pixelwise_rgb_color.py
