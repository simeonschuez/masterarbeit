#!/bin/bash

cd ../Preprocessing/type_color
python preprocessing-types.py

cd ../
python filter_test_dev_resample.py
python train_resample.py

cd ../Modelle/histogram_color/
python perceptron-train.py
python perceptron-predict.py

cd ../type_color
python naive-bayes-classifier-train.py
python naive-bayes-classifier-predict.py
