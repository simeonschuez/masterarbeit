import pandas as pd
import numpy as np
import os
import sys
import configparser
import collections
import matplotlib.pyplot as plt
from imblearn.under_sampling import RandomUnderSampler
from sklearn.metrics import classification_report, accuracy_score, balanced_accuracy_score

sys.path.append(os.path.abspath('../../Module'))
import preprocessing
import evaluation
import visualize

config = configparser.ConfigParser()
config.read('../../config.ini')

vg_json = config['PATHS']['vg-json']
vg_json_export = config['PATHS']['json-export']
image_dir = config['PATHS']['vg-images']

colors = preprocessing.basic_colors()
num_classes = 11
random_state = 123
max_entries = 10000

if __name__ == "__main__":
    for filename in ['baseline_arrays_bgr']:

        import_arrays = np.load(vg_json_export+filename+'.npz')

        train_x = import_arrays['train_x']
        train_y = import_arrays['train_y']
        dev_x = import_arrays['dev_x']
        dev_y = import_arrays['dev_y']
        test_x = import_arrays['test_x']
        test_y = import_arrays['test_y']

        print ('np-Arrays in {filename}:'.format(filename=filename))
        print ('shape train_x:',train_x.shape)
        print ('shape train_y:',train_y.shape)
        print ('shape dev_x:',dev_x.shape)
        print ('shape dev_y:',dev_y.shape)
        print ('shape test_x:',test_x.shape)
        print ('shape test_y:',test_y.shape)

        # Gleichverteilung für Dev/Test-Sets

        rus = RandomUnderSampler(random_state=random_state)

        #test_x, test_y = rus.fit_resample(test_x[:,1:], test_y[:,1:].argmax(axis=1))
        #test_y = np.eye(11)[test_y]
        #dev_x, dev_y = rus.fit_resample(dev_x[:,1:], dev_y[:,1:].argmax(axis=1))
        #dev_y = np.eye(11)[dev_y]

        ##############################
        # Undersampling für Train-Set
        ##############################

        # Farbkategorien als numerische Werte
        color_values = train_y[:,1:].argmax(axis=1)
        color_values.shape = (len(color_values),1)

        # DataFrame aus train_y
        df = pd.DataFrame(np.append(train_y, color_values, axis=1))
        # Instanzen pro Farbkategorie
        color_counts_dict = dict(df.groupby([12]).size().reset_index(name='count').values)

        # sample_df initialisieren
        sample_df = pd.DataFrame(columns=df.columns)

        # Über Farbwerte iterieren
        for i in list(range(0,11)):
            # Maximalwert für jeweilige Farbe überprüfen
            color_count = len(df.loc[df[12] == i])
            # Anzahl der Samples: max_entries wenn kleiner/gleich Anzahl Farbinstanzen, sonst Anzahl Farbinstanzen; Samples zu sample_df
            n = (max_entries if color_count >= max_entries else color_count)
            sample_df = sample_df.append(df.loc[df[12] == i].sample(n, replace=False, random_state=123))

        # Einträge aus Train-Set auswählen, deren ID in sample_df ist
        train_x = pd.DataFrame(train_x).loc[pd.DataFrame(train_x)[0].isin(sample_df[0].values)].to_numpy()
        train_y = pd.DataFrame(train_y).loc[pd.DataFrame(train_y)[0].isin(sample_df[0].values)].to_numpy()

        # Arrays in Datei schreiben
        export_filename = filename+'_res.npz'
        np.savez_compressed(
            vg_json_export+export_filename,
            train_x = train_x, train_y = train_y,
            test_x = test_x, test_y = test_y,
            dev_x = dev_x, dev_y = dev_y
        )

        print ('Arrays nach Resample:')
        print ('shape train_x:',train_x.shape)
        print ('shape train_y:',train_y.shape)
        print ('shape dev_x:',dev_x.shape)
        print ('shape dev_y:',dev_y.shape)
        print ('shape test_x:',test_x.shape)
        print ('shape test_y:',test_y.shape)
