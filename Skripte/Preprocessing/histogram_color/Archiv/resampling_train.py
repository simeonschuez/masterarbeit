import pandas as pd
import numpy as np
import sys
import os
import collections
import configparser
from sklearn.model_selection import train_test_split
from scipy.stats import entropy
from imblearn.under_sampling import RandomUnderSampler
from imblearn.over_sampling import RandomOverSampler
from nltk.corpus import wordnet as wn

sys.path.append(os.path.abspath('../../Module'))
import preprocessing

config = configparser.ConfigParser()
config.read('../../config.ini')
vg_json = config['PATHS']['vg-json']
data_dir = config['PATHS']['json-export']
image_dir = config['PATHS']['vg-images']

input_dir = data_dir + 'feature_arrays/'
output_dir = data_dir + 'feature_arrays/'

test_ratio = 0.2
random_state = 123
num_classes = 11

if __name__ == "__main__":

    for input_file in ['baseline_arrays_bgr', 'baseline_arrays_hsv', 'baseline_arrays_lab']:

        import_arrays = np.load(input_dir+input_file+'_filtered.npz')

        ### TRAIN-SET

        train_x = import_arrays['train_x']
        train_y = import_arrays['train_y']

        print ('{file}: Bearbeite Train-Set. Ursprüngliche Shapes:'.format(file=input_file), train_x.shape, train_y.shape)

        print ('durch Random Undersampling: Farben gleichverteilen')
        rus = RandomUnderSampler(random_state=random_state)
        train_x, train_y = rus.fit_resample(train_x, train_y[:,1:].argmax(axis=1))
        # train_y von Integer zu One-Hot-Encoding
        train_y = np.eye(num_classes)[train_y]
        # IDs zu train_y hinzufügen
        train_y = np.append(train_x[:,0:1], train_y, axis=1)

        try:
            not False in (train_x[:,0] == train_y[:,0])
        except:
            raise ValueError('{file}: Unterschiedliche Indizes im train-Set nach Resampling'.format(file=input_file))

        print ('{file}: Ergebnis train-Set-Shapes:'.format(file=input_file), train_x.shape, train_y.shape)

        outfile = output_dir+input_file+'_resampled.npz'
        print ('Erzeugte Arrays in Datei schreiben: '+ outfile)
        np.savez_compressed(
            outfile,
            train_x = train_x,
            train_y = train_y,
            dev_x = import_arrays['dev_x'],
            dev_y = import_arrays['dev_y'],
            test_x = import_arrays['test_x'],
            test_y = import_arrays['test_y']
        )
