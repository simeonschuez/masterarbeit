import cv2
import pandas as pd
import numpy as np
import os
import sys
import configparser
from sklearn.model_selection import train_test_split
from scipy.stats import entropy

sys.path.append(os.path.abspath('../../Module'))
import preprocessing

config = configparser.ConfigParser()
config.read('../../config.ini')

vg_json = config['PATHS']['vg-json']
vg_json_export = config['PATHS']['json-export']
image_dir = config['PATHS']['vg-images']

data_dir = vg_json_export + 'extracted_data/'

basic_colors = preprocessing.basic_colors()

size = False
test_ratio = 0.2
dev_ratio = 0.1
random_state = 123

if __name__ == "__main__":

    print('import source data frame')
    # DF importieren, Objektnamen als Liste
    objects = pd.read_csv(data_dir+"all_objects.csv", index_col=0)

    # Color Diagnostic Objects erhalten
    ###################################

    print('get color diagnostic objects')
    frequent_objects, _, _, _ = preprocessing.freq_cdo_cno(objects, num_cdos=100, num_cnos=100, min_num=100)

    # Training / Test-Split erhalten
    ###############################

    frequent_objects_df = objects.loc[objects.object_name.isin(frequent_objects)]

    print ('read train, dev and test sets')

    train_index = pd.read_csv(data_dir+"train_df.csv", index_col=0).index.intersection(frequent_objects_df.index)
    test_index = pd.read_csv(data_dir+"test_df.csv", index_col=0).index.intersection(frequent_objects_df.index)
    dev_index = pd.read_csv(data_dir+"dev_df.csv", index_col=0).index.intersection(frequent_objects_df.index)

    train_df = frequent_objects_df.loc[train_index]
    test_df = frequent_objects_df.loc[test_index]
    dev_df = frequent_objects_df.loc[dev_index]

    # Input- und Evaluations-Arrays erstellen
    #########################################

    print ('one hot encoding for color names')
    # One-Hot-Encdoing: Farbkategorien binär kodieren
    train_df = pd.get_dummies(train_df, prefix=['color'], columns=['color'])
    test_df = pd.get_dummies(test_df, prefix=['color'], columns=['color'])
    dev_df = pd.get_dummies(dev_df, prefix=['color'], columns=['color'])

    col_list = train_df.columns.tolist()

    # sample-DataFrame
    if size:
        test_size = int(size * test_ratio)
        dev_size = int(size * dev_ratio)
        training_size = int(size - (test_size + dev_size))
        print ('Size of Train-Split:', training_size)
        print ('Test-Ratio:',test_ratio)
        print ('Size of Test-Split:', test_size)
        print ('Dev-Ratio:', dev_ratio)
        print ('Size of Dev-Split:', dev_size)

        train_df = train_df.sample(n=training_size, random_state=random_state)
        test_df = test_df.sample(n=test_size, random_state=random_state)
        dev_df = dev_df.sample(n=dev_size, random_state=random_state)

    # reset index (for printing)
    train_df = train_df.reset_index(drop=False)
    test_df = test_df.reset_index(drop=False)
    dev_df = dev_df.reset_index(drop=False)

    # Histogramme berechnen, Arrays für x und y erstellen
    print ('computate histograms and build arrays for train split')
    #status.current_set = 'train split'
    train_x,train_y = preprocessing.x_y_histograms(train_df,image_dir,convert='lab')
    print ('computate histograms and build arrays for test split')
    #status.current_set = 'test split'
    test_x,test_y = preprocessing.x_y_histograms(test_df,image_dir,convert='lab')
    print ('computate histograms and build arrays for dev split')
    #status.current_set = 'train split'
    dev_x,dev_y = preprocessing.x_y_histograms(dev_df,image_dir,convert='lab')

    # Arrays in npz-Datei speichern
    export_filename = 'baseline_arrays_lab.npz'
    np.savez_compressed(
        vg_json_export+export_filename,
        train_x = train_x, train_y = train_y,
        test_x = test_x, test_y = test_y,
        dev_x = dev_x, dev_y = dev_y
    )

    # Ergebnisse anzeigen
    import_arrays = np.load(vg_json_export+export_filename)
    print ('results:')
    print ('shape train_x:',import_arrays['train_x'].shape)
    print ('shape train_y:',import_arrays['train_y'].shape)
    print ('shape test_x:',import_arrays['test_x'].shape)
    print ('shape test_y:',import_arrays['test_y'].shape)
    print ('shape dev_x:',import_arrays['dev_x'].shape)
    print ('shape dev_y:',import_arrays['dev_y'].shape)
