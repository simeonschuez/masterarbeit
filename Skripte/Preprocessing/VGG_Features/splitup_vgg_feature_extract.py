import cv2
import pandas as pd
import numpy as np
import os
import sys
import configparser

from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.applications.vgg16 import preprocess_input
from keras.applications.vgg16 import VGG16
from keras.models import Model


#sys.path.append(os.path.abspath('../../Module'))
#import preprocessing

config = configparser.ConfigParser()
config.read('../../config.ini')

vg_json = config['PATHS']['vg-json']
vg_json_export = config['PATHS']['json-export']
image_dir = config['PATHS']['vg-images']

data_dir = vg_json_export + 'extracted_data/'
output_dir = vg_json_export + 'feature_arrays/'
temp_output_dir = output_dir + 'temp/'
input_dir = vg_json_export + 'feature_arrays/'

size = False
test_ratio = 0.1
dev_ratio = 0.2
random_state = 123
num_splits = 4


class out():
    x = np.empty((0,4097))
    x_temp = np.empty((0,4097))
    y = np.empty((0,12))
    y_temp = np.empty((0,12))

class status():
    state = None

def url(series, directory):
    return (directory + str(series.image_id) + '.jpg')

def preprocess_image(series, directory):

    # load an image from file
    input_image = load_img(url(series, directory))
    # convert the image pixels to a numpy array
    image = img_to_array(input_image)
    bb = {
        'h':series.bb_h,
        'w':series.bb_w,
        'x':series.bb_x,
        'y':series.bb_y
    }

    # Preprocessing using OpenCV
    try:
        # Bereich in BoundingBox selektieren
        image = image[bb['y']:bb['y']+bb['h'],bb['x']:bb['x']+bb['w']]
        # Bild auf richtige Größe
        image = cv2.resize(image, (224,224))
        # Grayscale to RGB
        if len(image.shape) < 3:
            image = cv2.cvtColor(image,cv2.COLOR_GRAY2RGB)
    except Exception as ex:
        try:
            # Bild auf richtige Größe
            image = img_to_array(input_image)
            image = cv2.resize(image, (224,224))
            print('Error handling object {}. Resizing successfull. Error message: "{}"'.format(series.object_id, ex))
        except Exception as ex:
            print('Error handling object {}. Error message: "{}"'.format(series.object_id, ex))
    # reshape data for the model
    image = image.reshape((1, image.shape[0], image.shape[1], image.shape[2]))
    # prepare the image for the VGG model
    image = preprocess_input(image)

    return(image)

def vgg_features(series, model, directory):
    image = preprocess_image(series, image_dir)
    predict = model.predict(image)
    out_obj = np.append(series.name,predict).reshape(1,-1)

    out.x_temp = np.append(out.x_temp, out_obj, axis=0)
    if len(out.x_temp) >= 1000:
        out.x = np.append(out.x, out.x_temp, axis=0)
        out.x_temp = np.empty((0,4097))
        print (status.state,len(out.x))

def construct_vgg_array(df, model, directory):
    out.x = np.empty((0,4097))
    out.x_temp = np.empty((0,4097))

    df.apply(lambda x: vgg_features(x, model, directory), axis=1)

    return (np.append(out.x, out.x_temp, axis=0))

if __name__ == "__main__":
    for dir in [output_dir, temp_output_dir]:
        if not os.path.isdir(dir):
            os.mkdir(dir)
            print ('Pfad {path} angelegt'.format(path=dir))

    # Objekt-DataFrame importieren
    all_obj = pd.read_csv(vg_json_export+"extracted_data/all_objects.csv", index_col=0)
    # Gezippte Numpy-Archive importieren
    topdown_arrays = np.load(input_dir+'type_to_color_resampled.npz', allow_pickle=True)

    train_ids = topdown_arrays['train_y'][:,0]
    test_ids = topdown_arrays['test_x'][:,0]
    dev_ids = topdown_arrays['dev_y'][:,0]

    # VGG16-Modell laden
    vgg16 = VGG16(include_top=True)

    # Modell erstellen, der die Gewichte des letzten VGG16-Dense-Layers ausgibt
    model = Model(input=vgg16.input, output=vgg16.layers[-2].output)
    model.summary()

    status.state = 'Train'
    ids = topdown_arrays['train_y'][:,0]
    n = 0
    for split in np.split(ids,num_splits):
        df = all_obj.loc[split]
        print('Shape current split: {}'.format(str(df.shape)))
        features = construct_vgg_array(df, model, image_dir)
        np.save(temp_output_dir+'train_x_{}.npy'.format(str(n)), features)
        print ('Split {} complete. Shape: {}'.format(n,features.shape))
        n += 1
    print ('Train-Set Complete')

    status.state = 'Dev'
    ids = topdown_arrays['dev_y'][:,0]
    n = 0
    for split in [ids]:#np.split(ids,num_splits):
        df = all_obj.loc[split]
        print('Shape current split: {}'.format(str(df.shape)))
        features = construct_vgg_array(df, model, image_dir)
        np.save(temp_output_dir+'dev_x_{}.npy'.format(str(n)), features)
        print ('Split {} complete. Shape: {}'.format(n,features.shape))
        n += 1
    print ('Dev-Set Complete')

    status.state = 'Test'
    ids = topdown_arrays['test_y'][:,0]
    n = 0
    for split in [ids]:#np.split(ids,num_splits):
        df = all_obj.loc[split]
        print('Shape current split: {}'.format(str(df.shape)))
        features = construct_vgg_array(df, model, image_dir)
        np.save(temp_output_dir+'test_x_{}.npy'.format(str(n)), features)
        print ('Split {} complete. Shape: {}'.format(n,features.shape))
        n += 1
    print ('Test-Set Complete')
