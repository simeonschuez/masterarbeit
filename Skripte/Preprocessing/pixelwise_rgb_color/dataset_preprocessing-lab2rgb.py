import os
import sys
import pandas as pd
import numpy as np
import requests
import itertools
import math
import configparser
from scipy.io import loadmat
import skimage
import requests
from colormath.color_objects import LabColor, sRGBColor
from colormath.color_conversions import convert_color

config = configparser.ConfigParser()
config.read('../../config.ini')

sys.path.append(os.path.abspath('../../Module'))
import preprocessing

vg_json = config['PATHS']['vg-json']
vg_json_export = config['PATHS']['json-export']
image_dir = config['PATHS']['vg-images']

def gauss(x, mu=0.0, sigma=1.0):
    """
    Return the value of the Gaussian probability function with mean mu
    and standard deviation sigma at the given x value.
    """
    # aus https://introcs.cs.princeton.edu/python/22module/gaussian.py.html
    x = float(x - mu) / sigma
    return math.exp(-x*x/2.0) / math.sqrt(2.0*math.pi) / sigma

def lab_to_rgb(lab):
    l,a,b = lab
    lab = LabColor(l,a,b)
    rgb = convert_color(lab, sRGBColor).get_value_tuple()
    rgb = tuple(c*255 for c in rgb)
    #print (rgb)
    rgb = tuple(c if c <= 255 else 255. for c in rgb)
    return rgb

if __name__ == "__main__":

    ######################################
    # Chip-basiertes Datenset LAB -> RGB #
    ######################################

    # Textdatei abrufen
    r = requests.get('http://www.cvc.uab.es/color_naming/MembershipValues_CIELab.txt')
    # Text einlesen
    data = r.text
    # Zeilen trennen
    data = data.split('\r\n')
    # letzte Zeile ist leer
    data = data[:-1]

    # DataFrame erstellen mit Columns aus Text-Datei
    # vgl. http://www.cvc.uab.es/color_naming/
    columns = 'l a b red orange brown yellow green blue purple pink white gray black'.split()
    columns_reordered = 'l a b'.split() + preprocessing.basic_colors()
    df_lab = pd.DataFrame(columns=columns)

    # Zeile für Zeile zu DataFrame hinzufügen
    for line in data:
        s = pd.Series(line.split('\t'), index = columns)
        df_lab = df_lab.append(s,ignore_index=True)

    # Type der Einträge von str zu float64
    df_lab = df_lab.astype('float64')

    df_rgb_converted = pd.DataFrame.from_records(
        list(df_lab.apply(lambda x: lab_to_rgb((x.l, x.a, x.b)), axis=1))
                             , columns='r g b'.split())
    df_lab_rgb = pd.merge(df_rgb_converted, df_lab.drop('l a b'.split(),axis=1), left_index=True, right_index=True)

    df = df_lab_rgb

    # geordnete Liste mit RGB-Bins initialisieren
    l = []
    n = 3.5
    for i in range(32):
        l.append(n)
        n += 8
    rgb_bins = list(itertools.product(l,l,l))
    rgb_bins = sorted(rgb_bins, key = lambda x: (x[2], x[1], x[0]))

    chip_w2c = np.empty((0, 14))
    # über RGB-Bins iterieren
    for rgb_i in rgb_bins:
        # über RGB-Werte und Wahrscheinlichkeitsverteilungen (Chips) iterieren

        # np-Array für Wahrscheinlichkeitsverteilung über Farbnamen initialisieren
        p_i = np.zeros(11)

        for j in df.to_numpy():
            # j-Array: RGB-Werte von Wahrscheinlichkeitswerten trennen
            rgb_j = j[:3]
            p_j = j[3:]
            # Euklidische Distanz zwischen RGB-Werten von i und j
            distance = np.linalg.norm(rgb_i-rgb_j)
            p_j = p_j * gauss(distance, sigma=5)
            #p_j = p_j / len(df.to_numpy())
            p_i += p_j
        p_i = p_i / p_i.sum()
        i = np.append(rgb_i, p_i)
        chip_w2c = np.append(chip_w2c,[i], axis=0)

    chip_w2c_df = pd.DataFrame(chip_w2c, columns=columns)
    chip_w2c_df = chip_w2c_df[columns_reordered]

    # als np-Array exportieren
    chip_w2c_lab_arr = chip_w2c_df.to_numpy()


    #######################################
    # Datensets als np-Arrays exportieren #
    #######################################

    loaded = np.load(vg_json_export+'w2c_data.npz')
    w2c_arr = loaded['w2c']
    chip_w2c_arr = loaded['chip_w2c']

    np.savez_compressed(vg_json_export+'w2c_data.npz',
        w2c = w2c_arr,
        chip_w2c = chip_w2c_arr,
        chip_w2c_lab = chip_w2c_lab_arr
        )

    loaded = np.load(vg_json_export+'w2c_data.npz')

    print ('w2c-Array:', loaded['w2c'].shape)
    print ('chip-based w2c-Array:', loaded['chip_w2c'].shape)
    print ('Lab-chip-based w2c-Array:', loaded['chip_w2c_lab'].shape)
