import pandas as pd
import numpy as np
import sys
import os
import configparser
from sklearn.model_selection import train_test_split
from scipy.stats import entropy

sys.path.append(os.path.abspath('../../Module'))
import preprocessing

config = configparser.ConfigParser()
config.read('../../config.ini')
vg_json = config['PATHS']['vg-json']
vg_json_export = config['PATHS']['json-export']
image_dir = config['PATHS']['vg-images']

data_dir = vg_json_export + 'extracted_data/'
output_dir = vg_json_export + 'raw_feature_arrays/'


sample_size = False
random_state = 123

def empty_object_name(obj):
    if obj=='':
        return False
    return True

if __name__ == "__main__":

    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
        print ('Pfad {path} angelegt'.format(path=output_dir))

    print ('load DataFrame')

    objects = pd.read_csv(data_dir+"all_objects.csv", index_col=0)
    
    l = len(objects)
    objects = objects.loc[objects.apply(lambda x: type(x.object_name) == str and x.object_name != '', axis=1)]
    print ('sorted out {} objects with invalid names'.format(l - len(objects)))
    
    # Color Diagnostic Objects erhalten
    ###################################

    print('get frequent objects')

    frequent_objects,_,_,_ = preprocessing.freq_cdo_cno(objects, num_cdos=100, num_cnos=100, min_num=100)

    # Data Frame mit frequenten Objekten

    frequent_objects_df = objects['color object_name'.split()]\
        .loc[objects.object_name.isin(frequent_objects)]

    #print ('sort out objects with empty names')
    ## Namen mit leeren Objektnamen aussortieren (resultiert aus Entfernen von Farbbegriffen)
    #frequent_objects_df['valid_name'] = frequent_objects_df.apply(lambda x:empty_object_name(x.object_name), axis=1)
    #l = len(frequent_objects_df)
    #frequent_objects_df = frequent_objects_df.loc[frequent_objects_df.valid_name == True].drop(['valid_name'], axis=1)
    #l = l - len(frequent_objects_df)
    #print ('sorted out {} objects'.format(l))
    
    # One-Hot-Encoding für Objekt-Typen

    print ('one-hot encode object types')
    frequent_objects_df['_types'] = frequent_objects_df['object_name']
    frequent_objects_df = pd.get_dummies(frequent_objects_df,columns=['_types'], prefix='type')

    # Sample erhalten aus frequent_objects_df

    if sample_size:
        frequent_objects_df = frequent_objects_df.sample(sample_size, random_state=random_state)

    # Splitten in Train/Test/Dev-Sets
    print ('create train and test data set')

    # Indizes der jeweiligen Splits erhalten durch Intersection mit frequent_objects_df
    train_index = pd.read_csv(data_dir+"train_df.csv", index_col=0).index.intersection(frequent_objects_df.index)
    test_index = pd.read_csv(data_dir+"test_df.csv", index_col=0).index.intersection(frequent_objects_df.index)
    dev_index = pd.read_csv(data_dir+"dev_df.csv", index_col=0).index.intersection(frequent_objects_df.index)

    # DataFrames für Train/Test/Dev-Splits erstellen
    train_df = frequent_objects_df.loc[train_index]
    test_df = frequent_objects_df.loc[test_index]
    dev_df = frequent_objects_df.loc[dev_index]

    print ('Train set shape:', train_df.shape)
    print ('Test set shape:', test_df.shape)
    print ('Dev set shape:', dev_df.shape)

    # drop object_name column
    [train_df, test_df, dev_df] = [df.drop(columns=['object_name']) for df in [train_df, test_df, dev_df]]

    # One-Hot-Encoding für Objekt-Farben

    print ('one-hot encode object colors')

    train_y, test_y, dev_y = [df['color'] for df in [train_df,test_df, dev_df]]
    train_y, test_y, dev_y = [pd.get_dummies(df, prefix='color') for df in [train_y,test_y, dev_y]]
    for df in [train_y,test_y, dev_y]:
        df.insert(0, 'index', df.index, allow_duplicates=False)

    # Color-Column mit nominalen Daten löschen
    train_x, test_x, dev_x = [df.drop(columns=['color']) for df in [train_df, test_df, dev_df]]
    # Index als Column duplizieren
    for df in [train_x, test_x, dev_x]:
        df.insert(0, 'index', df.index, allow_duplicates=False)

    # In Numpy-Arrays konvertieren
    print ('convert DataFrames to numpy arrays')

    train_x, train_y, test_x, test_y, dev_x, dev_y = [df.to_numpy() for df in [train_x, train_y, test_x, test_y, dev_x, dev_y]]

    # Als Datei exportieren
    print ('write numpy arrays to file')

    export_filename = 'type_to_color.npz'
    np.savez_compressed(
        output_dir+export_filename,
        train_x = train_x,
        train_y = train_y,
        dev_x = dev_x,
        dev_y = dev_y,
        test_x = test_x,
        test_y = test_y
    )

    # Shapes der resultierenden Arrays ausgeben
    print ("shape train_x:",train_x.shape)
    print ("shape train_y:",train_y.shape)
    print ("shape test_x:",test_x.shape)
    print ("shape test_y:",test_y.shape)
    print ("shape dev_x:",dev_x.shape)
    print ("shape dev_y:",dev_y.shape)
