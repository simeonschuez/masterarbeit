import pandas as pd
import numpy as np
import sys
import os
import configparser
from sklearn.model_selection import train_test_split
from scipy.stats import entropy

sys.path.append(os.path.abspath('../Module'))
import preprocessing

config = configparser.ConfigParser()
config.read('../../config.ini')
vg_json = config['PATHS']['vg-json']
vg_json_export = config['PATHS']['json-export']
image_dir = config['PATHS']['vg-images']

test_ratio = 0.2

def no_color_in_name(name, color_list):
    if any(color in name for color in color_list):
        return False
    else:
        return True

if __name__ == "__main__":

    print ('load DataFrame')

    objects = pd.read_csv(vg_json_export+"objects_basic_color_terms.csv", index_col=0)

    # Color Diagnostic Objects erhalten
    ###################################

    print('get color diagnostic objects')
    # Objekte nach Häufigkeit
    object_names = objects.groupby('object_name').size().reset_index(name='count')
    # nur Objekte mit mindestens 100 Instanzen
    object_names = object_names.loc[object_names['count'] >= 100]
    frequent_objects = list(object_names.object_name)

    # Objektnamen als Id, one hot encdoing für Farben
    colors_per_object = objects\
        .drop(['image_id', 'object_id', 'bb_x', 'bb_y', 'bb_w', 'bb_h'], axis=1)\
        .pivot_table(index='object_name',
                   columns='color',
                   aggfunc=len,
                   fill_value=0)

    # Aussortieren: Nur noch frequente Objekte ohne Farbe im Namen
    colors_per_object = colors_per_object[colors_per_object.index.isin(frequent_objects)]
    colors_per_object = colors_per_object[colors_per_object.index.map(lambda x: no_color_in_name(x,preprocessing.basic_colors())).values]

    # Entropie berechnen
    colors_per_object['entropy'] = colors_per_object.apply(lambda x:preprocessing.calculate_entropy(x), axis=1)

    # Dict ausgeben: 100 Obekte mit niedrigster Entropie + bevorzugte Farbe
    color_diagnostic_objects = colors_per_object.sort_values('entropy').iloc[:100].idxmax(axis=1).to_dict()

    # Ende CDOs bestimmen
    #####################

    # Data Frame mit frequenten Objekten

    frequent_objects_df = objects['color object_name'.split()]\
        .loc[objects.object_name.isin(frequent_objects)]

    print ('one-hot encode object types')
    frequent_objects_df['_types'] = frequent_objects_df['object_name']
    frequent_objects_df = pd.get_dummies(frequent_objects_df,columns=['_types'], prefix='type')

    print ('split DataFrame into train and test set')

    train_df,test_df = train_test_split(frequent_objects_df, test_size=test_ratio, shuffle=False)

    print('extract color diagnostic objects from test set')
    # Color Diagnostic Objects aus Test-Set
    color_diagnostic_test_df = test_df.loc[test_df.object_name.isin(color_diagnostic_objects.keys())]

    # drop object_name column
    [train_df, test_df, color_diagnostic_test_df] = [df.drop(columns=['object_name']) for df in [train_df, test_df, color_diagnostic_test_df]]

    print ('one-hot encode object colors')

    train_y, test_y, cdo_test_y = [df['color'] for df in [train_df,test_df, color_diagnostic_test_df]]
    train_y, test_y, cdo_test_y = [pd.get_dummies(df, prefix='color') for df in [train_y,test_y, cdo_test_y]]
    for df in [train_y,test_y, cdo_test_y]:
        df.insert(0, 'index', df.index, allow_duplicates=False)

    # get input arrays
    train_x, test_x, cdo_test_x = [df.drop(columns=['color']) for df in [train_df,test_df, color_diagnostic_test_df]]
    for df in [train_x, test_x, cdo_test_x]:
        df.insert(0, 'index', df.index, allow_duplicates=False)

    print ('convert DataFrames to numpy arrays')

    train_x,train_y,test_x,test_y, cdo_test_x, cdo_test_y = [df.to_numpy() for df in [train_x,train_y,test_x,test_y, cdo_test_x, cdo_test_y]]

    print ('write numpy arrays to file')

    export_filename = 'object_to_color.npz'
    np.savez_compressed(
        vg_json_export+export_filename,
        train_x = train_x,
        train_y = train_y,
        test_x = test_x,
        test_y = test_y,
        cdo_test_x = cdo_test_x,
        cdo_test_y = cdo_test_y
    )

    print ("shape train_x:",train_x.shape)
    print ("shape train_y:",train_y.shape)
    print ("shape test_x:",test_x.shape)
    print ("shape test_y:",test_y.shape)
    print ("shape cdo_test_x:",cdo_test_x.shape)
    print ("shape cdo_test_y:",cdo_test_y.shape)
