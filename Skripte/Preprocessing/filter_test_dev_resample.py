import pandas as pd
import numpy as np
import sys
import os
import collections
import configparser
from sklearn.model_selection import train_test_split
from scipy.stats import entropy
from imblearn.under_sampling import RandomUnderSampler
from imblearn.over_sampling import RandomOverSampler
from nltk.corpus import wordnet as wn

sys.path.append(os.path.abspath('../Module'))
import preprocessing

config = configparser.ConfigParser()
config.read('../config.ini')
vg_json = config['PATHS']['vg-json']
data_dir = config['PATHS']['json-export']
image_dir = config['PATHS']['vg-images']

input_dir = data_dir + 'raw_feature_arrays/'
output_dir = data_dir + 'feature_arrays/'

test_ratio = 0.2
random_state = 123
num_classes = 11

def get_hyponyms(synset):
    """
    Quelle: https://stackoverflow.com/questions/15330725/how-to-get-all-the-hyponyms-of-a-word-synset-in-python-nltk-and-wordnet
    """
    hyponyms = set()
    hyponyms.update({synset})
    for hyponym in synset.hyponyms():
        hyponyms |= set(get_hyponyms(hyponym))
    return hyponyms | set(synset.hyponyms())

if __name__ == "__main__":

    all_obj = pd.read_csv(data_dir+"extracted_data/all_objects.csv", index_col=0)
    train_obj = pd.read_csv(data_dir+"extracted_data/train_df.csv", index_col=0)
    test_obj = pd.read_csv(data_dir+"extracted_data/test_df.csv", index_col=0)
    dev_obj = pd.read_csv(data_dir+"extracted_data/dev_df.csv", index_col=0)

    if not os.path.isdir(input_dir):
        raise Exception('Pfad {path} nicht gefunden'.format(path=input_dir))

    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
        print ('Pfad {path} angelegt'.format(path=output_dir))

    # Menschen raus
    person_hyponyms = get_hyponyms(wn.synset('person.n.01'))
    person_words = []
    for synset in person_hyponyms:
        person_words += [l.name().replace('_', ' ') for l in synset.lemmas()]
    no_person_index = all_obj.loc[np.logical_not(all_obj.object_name.isin(person_words))].index.to_list()

    # Konkurrierende Farbbezeichnungen raus
    ids_color_count = all_obj.groupby(['object_id', 'color']).size().groupby('object_id').size()
    no_multiple_colors_object_ids = ids_color_count.loc[ids_color_count == 1].keys().to_list()
    no_multiple_colors_index = all_obj.loc[all_obj['object_id'].isin(no_multiple_colors_object_ids)].index.to_list()

    # IDs von Frequenten Objekten
    frequent_objects, _, _, _ = preprocessing.freq_cdo_cno(all_obj, num_cdos=100, num_cnos=100, num_cbos=100, min_num=100, return_entropy=False)
    frequent_objects_index = all_obj.loc[all_obj.object_name.isin(frequent_objects)].index.to_list()

    # IDs in Datei schreiben
    np.savez(output_dir+'indizes.npz', no_person_index=np.array(no_person_index), no_multiple_colors_index=np.array(no_multiple_colors_index))

    # Arrays filtern + resamplen:

    test_ids = []
    dev_ids = []

    for input_file in ['type_to_color','baseline_arrays_bgr', 'baseline_arrays_hsv','baseline_arrays_lab']:
    # beginnen mit type_to_color; da nur frequente Items in diesem Set


        import_arrays = np.load(input_dir+input_file+'.npz')

        ### TRAIN-SET

        train_x = import_arrays['train_x']
        train_y = import_arrays['train_y']

        print ('{file}: Bearbeite Train-Set. Ursprüngliche Shapes:'.format(file=input_file), train_x.shape, train_y.shape)

        print ('Menschen ausfiltern')
        # Filtern: Keine Menschen im Trainset
        train_x = pd.DataFrame(train_x).loc[pd.DataFrame(train_x)[0].isin(no_person_index)].to_numpy()
        train_y = pd.DataFrame(train_y).loc[pd.DataFrame(train_y)[0].isin(no_person_index)].to_numpy()

        try:
            not False in (train_x[:,0] == train_y[:,0])
        except:
            raise ValueError('{file}: Unterschiedliche Indizes im Train-Set nach Resampling'.format(file=input_file))

        print ('{file}: Ergebnis Train-Set-Shapes:'.format(file=input_file), train_x.shape, train_y.shape)

        ### DEV-SET
        # Arrays aus Datei laden
        dev_x = import_arrays['dev_x']
        dev_y = import_arrays['dev_y']

        print ('{file}: Bearbeite Dev-Set. Ursprüngliche Shapes:'.format(file=input_file), dev_x.shape, dev_y.shape)

        if len(dev_ids) == 0:

            print ('auf frequente Objekte reduzieren')
            dev_x = pd.DataFrame(dev_x).loc[pd.DataFrame(dev_x)[0].isin(frequent_objects_index)].to_numpy()
            dev_y = pd.DataFrame(dev_y).loc[pd.DataFrame(dev_y)[0].isin(frequent_objects_index)].to_numpy()
            print ('Menschen ausfiltern')
            # Filtern: Keine Menschen + konkurrierende Farbwörter im Validierungsset
            dev_x = pd.DataFrame(dev_x).loc[pd.DataFrame(dev_x)[0].isin(no_person_index)].to_numpy()
            dev_y = pd.DataFrame(dev_y).loc[pd.DataFrame(dev_y)[0].isin(no_person_index)].to_numpy()
            print ('durch Random Undersampling: Farben gleichverteilen')
            rus = RandomUnderSampler(random_state=random_state)
            dev_x, dev_y = rus.fit_resample(dev_x, dev_y[:,1:].argmax(axis=1))
            # dev_y von Integer zu One-Hot-Encoding
            dev_y = np.eye(num_classes)[dev_y]
            # IDs zu dev_y hinzufügen
            dev_y = np.append(dev_x[:,0:1], dev_y, axis=1)

            dev_ids = dev_y[:,0].ravel()

        else:
            print ('Einträge nach dev_ids auswählen')
            dev_x = pd.DataFrame(dev_x).loc[pd.DataFrame(dev_x)[0].isin(dev_ids)].to_numpy()
            dev_y = pd.DataFrame(dev_y).loc[pd.DataFrame(dev_y)[0].isin(dev_ids)].to_numpy()

            instances_per_color = collections.Counter(dev_y[:,1:].argmax(axis=1)).values()
            if min(instances_per_color) != max(instances_per_color):
                print ('durch Random Undersampling: Farben gleichverteilen')
                rus = RandomUnderSampler(random_state=random_state)
                dev_x, dev_y = rus.fit_resample(dev_x, dev_y[:,1:].argmax(axis=1))
                # dev_y von Integer zu One-Hot-Encoding
                dev_y = np.eye(num_classes)[dev_y]
                # IDs zu dev_y hinzufügen
                dev_y = np.append(dev_x[:,0:1], dev_y, axis=1)


        try:
            not False in (dev_x[:,0] == dev_y[:,0])
        except:
            raise ValueError('{file}: Unterschiedliche Indizes im Dev-Set nach Resampling'.format(file=input_file))

        print ('{file}: Ergebnis Dev-Set-Shapes:'.format(file=input_file), dev_x.shape, dev_y.shape)

        ### TEST-SET
        # Arrays aus Datei laden
        test_x = import_arrays['test_x']
        test_y = import_arrays['test_y']

        print ('{file}: Bearbeite Test-Set. Ursprüngliche Shapes:'.format(file=input_file), test_x.shape, test_y.shape)

        if len(test_ids) == 0:

            print ('auf frequente Objekte reduzieren')
            test_x = pd.DataFrame(test_x).loc[pd.DataFrame(test_x)[0].isin(frequent_objects_index)].to_numpy()
            test_y = pd.DataFrame(test_y).loc[pd.DataFrame(test_y)[0].isin(frequent_objects_index)].to_numpy()
            print ('Menschen ausfiltern')
            # Filtern: Keine Menschen + konkurrierende Farbwörter im Testset
            test_x = pd.DataFrame(test_x).loc[pd.DataFrame(test_x)[0].isin(no_person_index)].to_numpy()
            test_y = pd.DataFrame(test_y).loc[pd.DataFrame(test_y)[0].isin(no_person_index)].to_numpy()
            print ('durch Random Undersampling: Farben gleichverteilen')
            rus = RandomUnderSampler(random_state=random_state)
            test_x, test_y = rus.fit_resample(test_x, test_y[:,1:].argmax(axis=1))
            # test_y von Integer zu One-Hot-Encoding
            test_y = np.eye(num_classes)[test_y]
            # IDs zu test_y hinzufügen
            test_y = np.append(test_x[:,0:1], test_y, axis=1)

            test_ids = test_y[:,0].ravel()

        else:
            print ('Einträge nach test_id auswählen')
            test_x = pd.DataFrame(test_x).loc[pd.DataFrame(test_x)[0].isin(test_ids)].to_numpy()
            test_y = pd.DataFrame(test_y).loc[pd.DataFrame(test_y)[0].isin(test_ids)].to_numpy()

            instances_per_color = collections.Counter(test_y[:,1:].argmax(axis=1)).values()
            if min(instances_per_color) != max(instances_per_color):
                print ('durch Random Undersampling: Farben gleichverteilen')
                rus = RandomUnderSampler(random_state=random_state)
                test_x, test_y = rus.fit_resample(test_x, test_y[:,1:].argmax(axis=1))
                # test_y von Integer zu One-Hot-Encoding
                test_y = np.eye(num_classes)[test_y]
                # IDs zu test_y hinzufügen
                test_y = np.append(test_x[:,0:1], test_y, axis=1)

        try:
            not False in (test_x[:,0] == test_y[:,0])
        except:
            raise ValueError('{file}: Unterschiedliche Indizes im Test-Set nach Resampling'.format(file=input_file))

        print ('{file}: Ergebnis Test-Set-Shapes:'.format(file=input_file), test_x.shape, test_y.shape)

        outfile = output_dir+input_file+'_filtered.npz'
        print ('Erzeugte Arrays in Datei schreiben: '+ outfile)
        np.savez_compressed(
            outfile,
            train_x = train_x,
            train_y = train_y,
            dev_x = dev_x,
            dev_y = dev_y,
            test_x = test_x,
            test_y = test_y
        )
