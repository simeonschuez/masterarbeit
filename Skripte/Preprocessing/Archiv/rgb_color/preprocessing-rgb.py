#import matplotlib.pyplot as plt
import matplotlib.patches as patches
import pandas as pd
import numpy as np
import sys
import os
import configparser
from sklearn.model_selection import train_test_split
from scipy.stats import entropy

sys.path.append(os.path.abspath('../../Module'))
import preprocessing
import visualize

config = configparser.ConfigParser()
config.read('../../config.ini')
vg_json = config['PATHS']['vg-json']
vg_json_export = config['PATHS']['json-export']
image_dir = config['PATHS']['vg-images']

data_dir = vg_json_export + 'extracted_data/'

color_columns = ['color_'+c for c in preprocessing.basic_colors()]

sample_size = False
random_state = 123

if __name__ == "__main__":

    objects = pd.read_csv(data_dir+"all_objects.csv", index_col=0)

    # Color Diagnostic Objects erhalten
    ###################################

    print('get color diagnostic objects')

    frequent_objects, color_diagnostic_objects, color_neutral_objects = preprocessing.freq_cdo_cno(objects, num_cdos=100, num_cnos=100, min_num=100)

    # DataFrame mit frequenten Objekten

    frequent_objects_df = objects.loc[objects.object_name.isin(frequent_objects)]

    if sample_size:
        frequent_objects_df = frequent_objects_df.sample(sample_size, random_state=random_state)

    df = frequent_objects_df#.sample(frac=1, random_state=123)
    # RGB-Werte extrahieren
    df['rgb'] = df.apply(lambda x: preprocessing.mean_rgb_value(x,image_dir,output='rgb')[:3], axis=1)
    # One-Hot-Encoding für Farbnamen
    df = pd.get_dummies(df, columns=['color'])

    print ('formatting dataframe')

    # Index als neue Column einfügen
    df.insert(0, 'index', df.index, allow_duplicates=False)
    # RGB-Werte in separate Columns aufsplitten
    df['r'] = df.apply(lambda x: x.rgb[0], axis=1)
    df['g'] = df.apply(lambda x: x.rgb[1], axis=1)
    df['b'] = df.apply(lambda x: x.rgb[2], axis=1)

    # Indizes der Train/Test/Dev-Splits durch Intersection mit Frequent Objects
    print ('create train and test data set')
    train_index = pd.read_csv(data_dir+"train_df.csv", index_col=0).index.intersection(df.index)
    test_index = pd.read_csv(data_dir+"test_df.csv", index_col=0).index.intersection(df.index)
    dev_index = pd.read_csv(data_dir+"dev_df.csv", index_col=0).index.intersection(df.index)

    # DataFrames für Splits erzeugen
    train_df = df.loc[train_index]
    test_df = df.loc[test_index]
    dev_df = df.loc[dev_index]

    # Color-Diagnostic-Objects und Color-Neutral-Objects
    #cdo_test = test_df.loc[test_df.object_name.isin(color_diagnostic_objects.keys())]
    #cno_test = test_df.loc[test_df.object_name.isin(color_neutral_objects)]

    print ('Train split shape:', train_df.shape)
    print ('Test split shape:', test_df.shape)
    print ('Dev split shape:', dev_df.shape)
    #print ('CDO split shape:', cdo_test.shape)
    #print ('CNO split shape:', cno_test.shape)


    print ('select relevant columns for input and output data')

    train_x = train_df[['index','r','g','b']]
    train_y = pd.get_dummies(train_df[['index']+color_columns])
    test_x = test_df[['index','r','g','b']]
    test_y = pd.get_dummies(test_df[['index']+color_columns])
    dev_x = dev_df[['index','r','g','b']]
    dev_y = pd.get_dummies(dev_df[['index']+color_columns])
    #cdo_test_x = cdo_test[['index','r','g','b']]
    #cdo_test_y = pd.get_dummies(cdo_test[['index']+color_columns])
    #cno_test_x = cno_test[['index','r','g','b']]
    #cno_test_y = pd.get_dummies(cno_test[['index']+color_columns])

    print ('convert to numpy arrays')

    #convert DataFrames to numpy arrays
    train_x,train_y,test_x,test_y,dev_x,dev_y = [dataframe.to_numpy() for dataframe in [train_x,train_y,test_x,test_y,dev_x,dev_y]]
    #,cdo_test_x,cdo_test_y,cno_test_x,cno_test_y

    print ('write arrays to file')

    export_filename = 'rgb_to_color.npz'
    np.savez_compressed(
        vg_json_export+export_filename,
        train_x = train_x,
        train_y = train_y,
        test_x = test_x,
        test_y = test_y,
        dev_x = dev_x,
        dev_y = dev_y
    #    cdo_test_x = cdo_test_x,
    #    cdo_test_y = cdo_test_y,
    #    cno_test_x = cno_test_x,
    #    cno_test_y = cno_test_y
    )

    print ("shape train_x:",train_x.shape)
    print ("shape train_y:",train_y.shape)
    print ("shape test_x:",test_x.shape)
    print ("shape test_y:",test_y.shape)
    print ("shape dev_x:",dev_x.shape)
    print ("shape dev_y:",dev_y.shape)
    #print ("shape cdo_test_x:",cdo_test_x.shape)
    #print ("shape cdo_test_y:",cdo_test_y.shape)
    #print ("shape cno_test_x:",cno_test_x.shape)
    #print ("shape cno_test_y:",cno_test_y.shape)
