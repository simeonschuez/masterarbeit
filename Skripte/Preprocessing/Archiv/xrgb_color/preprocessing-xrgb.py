import matplotlib.pyplot as plt
import matplotlib.patches as patches
import pandas as pd
import numpy as np
import sys
import os
import configparser
from sklearn.model_selection import train_test_split
from scipy.stats import entropy

sys.path.append(os.path.abspath('../../Module'))
import preprocessing
import visualize

config = configparser.ConfigParser()
config.read('../../config.ini')
vg_json = config['PATHS']['vg-json']
vg_json_export = config['PATHS']['json-export']
image_dir = config['PATHS']['vg-images']

test_ratio = 0.2

if __name__ == "__main__":

    objects = pd.read_csv(vg_json_export+"objects_basic_color_terms.csv", index_col=0)

    # Color Diagnostic Objects erhalten
    ###################################

    print('get color diagnostic objects')

    frequent_objects, color_diagnostic_objects, color_neutral_objects = preprocessing.freq_cdo_cno(objects, num_cdos=100, num_cnos=100, min_num=100)

    # DataFrame mit frequenten Objekten

    frequent_objects_df = objects.loc[objects.object_name.isin(frequent_objects)]

    df = frequent_objects_df.sample(frac=1, random_state=123)
    df['rgb'] = df.apply(lambda x: preprocessing.mean_rgb_value(x,image_dir,output='rgb')[:3], axis=1)
    df = pd.get_dummies(df, columns=['color'])
    color_columns = ['color_'+c for c in preprocessing.basic_colors()]

    print ('formatting dataframe')

    df.insert(0, 'index', df.index, allow_duplicates=False)
    df['r'],df['g'],df['b']  = df.apply(lambda x:x.rgb[0],axis=1),df.apply(lambda x:x.rgb[1],axis=1),df.apply(lambda x:x.rgb[2],axis=1)
    train,test=train_test_split(df,test_size=test_ratio,shuffle=False)
    #cdo_test = test.loc[test.object_name.isin(color_diagnostic_objects.keys())]

    print ('splitting train and test sets')

    train_x = train[['index','r','g','b']]
    train_y = pd.get_dummies(train[['index']+color_columns])
    test_x = test[['index','r','g','b']]
    test_y = pd.get_dummies(test[['index']+color_columns])
    #cdo_test_x = cdo_test[['index','r','g','b']]
    #cdo_test_y = pd.get_dummies(cdo_test[['index']+color_columns])

    print ('convert to numpy arrays')

    #convert DataFrames to numpy arrays
    train_x,train_y,test_x,test_y = [dataframe.to_numpy() for dataframe in [train_x,train_y,test_x,test_y]]
    # , cdo_test_x, cdo_test_y

    print ('write arrays to file')

    export_filename = 'rgb_to_color.npz'
    np.savez_compressed(
        vg_json_export+export_filename,
        train_x = train_x,
        train_y = train_y,
        test_x = test_x,
        test_y = test_y,
    #    cdo_test_x = cdo_test_x,
    #    cdo_test_y = cdo_test_y
    )

    print ("shape train_x:",train_x.shape)
    print ("shape train_y:",train_y.shape)
    print ("shape test_x:",test_x.shape)
    print ("shape test_y:",test_y.shape)
    #print ("shape cdo_test_x:",cdo_test_x.shape)
    #print ("shape cdo_test_y:",cdo_test_y.shape)
