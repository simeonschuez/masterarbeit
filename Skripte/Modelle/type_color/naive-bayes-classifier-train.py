import numpy as np
np.random.seed(123)

import pandas as pd
import configparser
import os
from joblib import dump, load
from sklearn.naive_bayes import BernoulliNB

config = configparser.ConfigParser()
config.read('../../config.ini')
data_dir = config['PATHS']['json-export']
import_dir = data_dir+'feature_arrays/'
output_dir = data_dir + 'models/'

if not os.path.isdir(output_dir):
    os.mkdir(output_dir)
    print ('Pfad {path} angelegt'.format(path=output_dir))

print ('input-arrays importieren')
import_arrays = np.load(import_dir+'type_to_color_resampled.npz')

train_x = import_arrays['train_x']
print ('train_x:',train_x.shape)
train_y = import_arrays['train_y']
print ('train_y:',train_y.shape)
dev_x = import_arrays['dev_x']
print ('dev_x:',dev_x.shape)
dev_y = import_arrays['dev_y']
print ('dev_y:',dev_y.shape)

print ('arrays formatieren')
# Indizes abtrennen
train_y_index = train_y[:,0:1]
train_y = train_y[:,1:]
dev_y_index = dev_y[:,0:1]
dev_y = dev_y[:,1:]

train_x = train_x[:,1:]
dev_x = dev_x[:,1:]

# One-Hot-Kodierung -> Int
train_y = train_y.argmax(axis=1)
dev_y = dev_y.argmax(axis=1)

print ('initialisieren, fitten, evaluieren des klassifikators')

classifier = BernoulliNB()
classifier.fit(train_x, train_y)
# Accuracy auf Dev-Set
score_all = classifier.score(dev_x,dev_y)
print ('Score:', score_all)
dump(classifier, output_dir+'topdown_naive_bayes.joblib')
