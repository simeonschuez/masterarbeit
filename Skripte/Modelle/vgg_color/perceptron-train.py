import numpy as np
np.random.seed(123)

import matplotlib.pyplot as plt

import keras
from keras.models import Model
from keras.layers import Input, Dense, Dropout
from keras.optimizers import RMSprop
from keras.utils import plot_model

import configparser
import os

config = configparser.ConfigParser()
config.read('../../config.ini')

data_dir = config['PATHS']['json-export']
input_dir = data_dir+'feature_arrays/'
output_dir = data_dir + 'models/'
batch_size = 128
num_classes = 11
epochs = 25
input_dim = 4096

if not os.path.isdir(output_dir):
    os.mkdir(output_dir)
    print ('Pfad {path} angelegt'.format(path=output_dir))

# the data, split between train and test sets
input_file = 'vgg16_features.npz'
print ('input dir:', input_file)
import_arrays = np.load(input_dir+input_file, allow_pickle=True)

# exclude 1st column from every array (contains id)
x_train = import_arrays['train_x'][:,1:]
y_train = import_arrays['train_y'][:,1:]
x_test = import_arrays['dev_x'][:,1:]
y_test = import_arrays['dev_y'][:,1:]

print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

######################################

# Inputs
inputs = Input(shape=(input_dim,))
x = Dense(240, activation='relu')(inputs)
x = Dropout(0.2)(x)
x = Dense(24, activation='relu')(x)
x = Dropout(0.2)(x)

predictions = Dense(num_classes, activation='softmax', name='predictions')(x)

model = Model(inputs=inputs, outputs=predictions)

plot_model(model, to_file=data_dir+'images/vgg16_simple.png', show_shapes=True, show_layer_names=False)

model.summary()

model.compile(loss='categorical_crossentropy',
              optimizer=RMSprop(lr=0.0001),
              metrics=['accuracy'])

history = model.fit(x_train, y_train,
                    batch_size=batch_size,
                    epochs=10,
                    verbose=1,
                    validation_data=(x_test, y_test))

model_file = 'vgg16_simple.h5'
print ('saving model to', model_file)
model.save(output_dir+model_file)

########################################

print ('Import object information')
input_file = 'type_to_color_resampled.npz'
print ('input file:', input_file)
import_arrays_types = np.load(input_dir+input_file, allow_pickle=True)
# exclude 1st column from every array (contains id)
types_train = import_arrays_types['train_x'][:,1:]
types_test = import_arrays_types['dev_x'][:,1:]

# Inputs
inputs = Input(shape=(input_dim,))
x = Dropout(0.2)(inputs)
object_predictions = Dense(872, activation='softmax', name='classes')(x)
x = Dropout(0.2)(object_predictions)
x = Dense(240, activation='relu')(x)
x = Dropout(0.2)(x)
x = Dense(24, activation='relu')(x)
x = Dropout(0.2)(x)
color_predictions = Dense(num_classes, activation='softmax', name='colors')(x)

model = Model(inputs=inputs, outputs=[object_predictions, color_predictions])

plot_model(model, to_file=data_dir+'images/vgg16_jointlearning.png', show_shapes=True, show_layer_names=False)

model.summary()

model.compile(loss='categorical_crossentropy',
              optimizer=RMSprop(lr=0.0001),
              metrics=['accuracy'])

history = model.fit(x_train, [types_train, y_train],
                    batch_size=batch_size,
                    epochs=15,
                    verbose=1,
                    validation_data=(x_test, [types_test, y_test]))

model_file = 'vgg16_jointlearning.h5'
print ('saving model to', model_file)
model.save(output_dir+model_file)
