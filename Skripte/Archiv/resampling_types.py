import pandas as pd
import numpy as np
import sys
import os
import collections
import configparser
from sklearn.model_selection import train_test_split
from scipy.stats import entropy
from imblearn.under_sampling import RandomUnderSampler
from imblearn.over_sampling import RandomOverSampler

sys.path.append(os.path.abspath('../../Module'))
import preprocessing

config = configparser.ConfigParser()
config.read('../../config.ini')
vg_json = config['PATHS']['vg-json']
vg_json_export = config['PATHS']['json-export']
input_dir = vg_json_export + 'feature_arrays/'
output_dir = vg_json_export + 'feature_arrays/'

image_dir = config['PATHS']['vg-images']

test_ratio = 0.2

if __name__ == "__main__":

    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
        print ('Pfad {path} angelegt'.format(path=output_dir))

    print ('load DataFrame')

    all_objects = pd.read_csv(vg_json_export+"extracted_data/all_objects.csv", index_col=0)
    train_objects = pd.read_csv(vg_json_export+"extracted_data/train_df.csv", index_col=0)
    dev_objects = pd.read_csv(vg_json_export+"extracted_data/dev_df.csv", index_col=0)

    arrays = np.load(input_dir+'type_to_color_filtered.npz')

    print ('load np-Arrays')

    train_x = arrays['train_x']
    train_y = arrays['train_y']

    print ('processing train set')

    # OneHot Encoding in Int umwandeln
    train_x = np.append(train_x[:,0:1],train_x[:,1:].argmax(axis=1).reshape(-1,1),axis=1)
    train_y = np.append(train_y[:,0:1],train_y[:,1:].argmax(axis=1).reshape(-1,1),axis=1)

    # Mit RandomUnderSampler: Gleichverteilung über Objekttypen herstellen (100 Instanzen pro Typ)

    rus = RandomUnderSampler(random_state=123, sampling_strategy='all')
    train_y, X_res = rus.fit_resample(train_y, train_x[:,1:])

    train_x = np.append(train_y[:,0:1], X_res, axis=1)

    # Mit RandomOverSampler: Gleichverteilung über Farbwörter herstellen

    ros = RandomOverSampler(random_state=123, sampling_strategy='all')
    train_x,y_res = ros.fit_resample(train_x, train_y[:,1:])

    train_y = np.append(train_x[:,0:1], y_res.reshape(-1,1), axis=1)

    # Farben und Typen als One-Hot-Vektoren

    number_types = train_x[:,1:].max()+1
    number_colors = train_y[:,1:].max()+1

    train_x = np.append(train_x[:,0:1],np.eye(number_types)[train_x[:,1]], axis=1)
    train_y = np.append(train_y[:,0:1],np.eye(number_colors)[train_y[:,1]], axis=1)

    #print ('processing dev and test arrays')
    # Dev und Test Arrays: Gleichmäßige Verteilung für Farben
    #dev_x = arrays['dev_x']
    #dev_y = arrays['dev_y']
    #test_x = arrays['test_x']
    #test_y = arrays['test_y']
    #dev_x, y_res = rus.fit_resample(dev_x, dev_y[:,1:].argmax(axis=1))
    #dev_y = np.append(dev_x[:,0:1],np.eye(number_colors)[y_res], axis=1)
    #test_x, y_res = rus.fit_resample(test_x, test_y[:,1:].argmax(axis=1))
    #test_y = np.append(test_x[:,0:1],np.eye(number_colors)[y_res], axis=1)
    #print ('Shapes:\n train_x: {}, dev_x: {}, test_x:{}'.format(train_x.shape, dev_x.shape, test_x.shape))

    filename = 'type_to_color_resampled.npz'
    print ('writing to file '+output_dir+filename)
    np.savez_compressed(
            output_dir+filename,
            train_x = train_x,
            train_y = train_y,
            dev_x = arrays['dev_x'],
            dev_y = arrays['dev_y'],
            test_x = arrays['test_x'],
            test_y = arrays['test_y']
        )
