'''Trains a simple deep NN on the MNIST dataset.
Gets to 98.40% test accuracy after 20 epochs
(there is *a lot* of margin for parameter tuning).
2 seconds per epoch on a K520 GPU.
'''

import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import RMSprop
import configparser
import numpy as np

config = configparser.ConfigParser()
config.read('../../config.ini')

data_dir = config['PATHS']['json-export']

batch_size = 128
num_classes = 11
epochs = 10

# the data, split between train and test sets

import_arrays = np.load(data_dir+'type_to_color.npz')

# exclude 1st column from every array (contains id)
x_train = import_arrays['train_x'][:,1:]
y_train = import_arrays['train_y'][:,1:]
x_test = import_arrays['test_x'][:,1:]
y_test = import_arrays['test_y'][:,1:]

print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

## convert class vectors to binary class matrices
#y_train = keras.utils.to_categorical(y_train, num_classes)
#y_test = keras.utils.to_categorical(y_test, num_classes)

model = Sequential()
model.add(Dense(796, activation='relu', input_shape=(796,)))
model.add(Dropout(0.2))
model.add(Dense(240, activation='relu'))
model.add(Dropout(0.2))
#model.add(Dense(24, activation='relu'))
#model.add(Dropout(0.2))
model.add(Dense(num_classes, activation='softmax'))

model.summary()

model.compile(loss='categorical_crossentropy',
              optimizer=RMSprop(),
              metrics=['accuracy'])

history = model.fit(x_train, y_train,
                    batch_size=batch_size,
                    epochs=epochs,
                    verbose=1,
                    validation_data=(x_test, y_test))

score = model.evaluate(x_test, y_test, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])

model.save(data_dir+'type-color-perceptron.h5')
