import cv2
import os
import pandas as pd
import numpy as np
import preprocessing
import configparser
import datetime
import string
from sklearn.metrics import confusion_matrix, precision_recall_fscore_support, accuracy_score, label_ranking_average_precision_score, precision_score, recall_score, f1_score

from pprint import pprint
import matplotlib.pyplot as plt

config = configparser.ConfigParser()
config.read('../../config.ini')

vg_json = config['PATHS']['vg-json']
vg_json_export = config['PATHS']['json-export']
image_dir = config['PATHS']['vg-images']
image_out = vg_json_export + 'images/'


def plot_confusion_matrix(y_true, y_pred, classes,
                          error_matrix = False,
                          normalize=True,
                          title=None,
                          colorbar = True,
                          cmap=plt.cm.Blues,
                          save_image = False,
                          filename = False,
                          file_dir = False,
                          add_date = False):
    # Quelle: https://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html

    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        elif title == False:
            title = ''
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    if error_matrix:
        np.fill_diagonal(cm, 0)

    fig, ax = plt.subplots(figsize=(7,7))
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    if colorbar:
        ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()

    # Bild speichern
    if save_image:
        if not file_dir:
            raise Exception('kein Pfad angegeben')
            return
        else:
            if not os.path.isdir(file_dir):
                os.mkdir(file_dir)
                print ('Pfad {path} angelegt'.format(path=file_dir))

        if not filename:
            diagram_type = '_errors_' if error_matrix else '_predictions_'
            filename = title if title else y_pred.name + diagram_type
            filename = filename + str(datetime.datetime.now()).replace(' ','_') if add_date else filename
            for char in string.punctuation.replace('_',''):
                filename = filename.replace(char, '')
            filename = filename.replace(' ', '_')
            filename += '.png'

        plt.savefig(file_dir+filename,bbox_inches="tight")

    #return ax
    plt.show()

def print_scores(name, df, df_cdo = False, df_cno = False, df_cbo = False, column=False, return_dict = False, metric='accuracy', average='weighted'):
    if not column:
        column = name
    print('{n}:'.format(n=name))

    result_list = []

    if metric.lower() == 'accuracy':
        get_score = lambda x,y: accuracy_score(x,y)
    elif metric.lower() == 'precision':
        get_score = lambda x,y: precision_score(x,y, average=average)
    elif metric.lower() == 'recall':
        get_score = lambda x,y: recall_score(x,y, average=average)
    elif metric.lower() == 'f1':
        get_score = lambda x,y: f1_score(x,y, average=average)
    else:
        raise Exception('Falsche Angabe für metric:',metric)

    print ('Metric:', metric)

    score_all = get_score(df['color'], df[column])
    all_res = {'Alle Objekte':score_all}
    print (all_res)
    result_list.append(all_res)

    if type(df_cdo) != bool:
        score_cdo = get_score(df_cdo['color'], df_cdo[column])
        cdo_res = {'Color-Diagnostic Objects': score_cdo}
        print (cdo_res)
        result_list.append(cdo_res)
    if type(df_cbo) != bool:
        score_cbo = get_score(df_cbo['color'], df_cbo[column])
        cbo_res = {'Color-Biased Objects': score_cbo}
        print (cbo_res)
        result_list.append(cbo_res)
    if type(df_cno) != bool:
        score_cno = get_score(df_cno['color'], df_cno[column])
        cno_res = {'Color-Neutral Objects': score_cno}
        print (cno_res)
        result_list.append(cno_res)
    print ('\n')

    # Scores als Dict ausgeben
    if return_dict:
        results_dict = {}
        for result in result_list:
            results_dict.update(result)
        return ({name: results_dict})

def evaluation_metrics(column, df, verbose=True, labels=preprocessing.basic_colors(), average='weighted'):


    accuracy = accuracy_score(df['color'], df[column])
    precision,recall,fbeta_score,_ = precision_recall_fscore_support(df.color, df[column],  labels=labels, average=average, warn_for=('precision', 'recall', 'f-score'))

    if verbose:
        # Evaluation für einzelne Labels
        pr,re,fs,su = precision_recall_fscore_support(df.color, df[column],  labels=labels, average=None, warn_for=('precision', 'recall', 'f-score'))

        print ('precision:')
        pprint (dict(zip(labels, pr)))

        print ('recall:')
        pprint (dict(zip(labels, re)))

        print ('fbeta_score:')
        pprint (dict(zip(labels, fs)))
        if verbose == 'all':
            print ('support:')
            # Anzahl der Instanzen pro Klasse in y_true
            pprint (dict(zip(labels, su)))

    print('\n'+'accuracy: {}'.format(accuracy))
    print('mean precision ('+average+'): {}'.format(precision))
    print('mean recall ('+average+'): {}'.format(recall))
    print('mean fbeta_score ('+average+'): {}'.format(fbeta_score))

    return precision,recall,fbeta_score

def get_label_ranking_average_precision_score(y_true, y_score):

    if type(y_true) == pd.core.series.Series:
        y_true = pd.get_dummies(y_true).T.reindex(preprocessing.basic_colors()).T.fillna(0).to_numpy().astype(int)
    return label_ranking_average_precision_score(y_true, y_score, sample_weight = None)
